#!/home/lindsb/usr/anaconda2/bin/python

from combine_tec_avgs import combine

in_dirs = ['equil-prod-1', 'prod-2']
prod_its = [100000, 200000]
avg_files = ['avg_rhoda.tec', 'avg_rho_fld_np.tec', 'avg_rho_fld_np_c.tec']
combine(in_dirs, prod_its, avg_files)
