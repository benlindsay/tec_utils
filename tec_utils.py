r"""tec_utils is a module containing some useful functions for analyzing and
manipulating data in .tec files

"""

#==============================================================================
#
#   Module:     combine_tec_avgs.py
#   Author:     Ben Lindsay
#
#==============================================================================

from __future__ import division
import numpy as np
from itertools import islice, cycle
import os
import glob
import matplotlib.pyplot as plt
import mpl_custom as mplc

class struct_3d():
    """
    Convenient structure to keep 3D density data from 1 .tec file together
    """ 
    def __init__(self, x, y, z, real, imag):
        self.check_inputs(x, y, z, real, imag)
        self.x = x
        self.y = y
        self.z = z
        self.real = real
        self.imag = imag
    def check_inputs(self, x, y, z, real, imag):
        if real.shape != imag.shape:
            raise ValueError, ("dimensions of 'real' don't match "
                               "dimensions of'imag'")
        if x.ndim!=1 or x.size!=real.shape[0]: 
            raise ValueError, ("'x' must be a 1D array with length matching "
                               "length of axis 0 of 'real'")
        if y.ndim!=1 or y.size!=real.shape[1]: 
            raise ValueError, ("'y' must be a 1D array with length matching "
                               "length of axis 1 of 'real'")
        if z.ndim!=1 or z.size!=real.shape[2]: 
            raise ValueError, ("'z' must be a 1D array with length matching "
                               "length of axis 2 of 'real'")
        if not all(x==np.sort(x)):
            raise ValueError, "x is not sorted"
        if not all(y==np.sort(y)):
            raise ValueError, "y is not sorted"
        if not all(z==np.sort(z)):
            raise ValueError, "z is not sorted"
    def get_core(self, xscale=0.5, yscale=0.5, zscale=1):
        if xscale < 0 or xscale > 1:
            raise ValueError, "'xscale' must be between 0 and 1"
        if yscale < 0 or yscale > 1:
            raise ValueError, "'yscale' must be between 0 and 1"
        if zscale < 0 or zscale > 1:
            raise ValueError, "'zscale' must be between 0 and 1"
        nx = self.x.size
        ny = self.y.size
        nz = self.z.size
        xcut = int( np.ceil((1 - xscale) * ((nx-1) // 2) ) )
        ycut = int( np.ceil((1 - yscale) * ((ny-1) // 2) ) )
        zcut = int( np.ceil((1 - zscale) * ((nz-1) // 2) ) )
        xinds = range(xcut, nx-xcut)
        yinds = range(ycut, ny-ycut)
        zinds = range(zcut, nz-zcut)
        xcore = self.x[xinds]
        ycore = self.y[yinds]
        zcore = self.z[zinds]
        realcore = self.real[np.ix_(xinds, yinds, zinds)]
        imagcore = self.imag[np.ix_(xinds, yinds, zinds)]
        return struct_3d(xcore, ycore, zcore, realcore, imagcore)
    def get_midline(self):
        nx = self.x.size
        ny = self.y.size
        return self.z, self.real[nx//2, ny//2, :]

#------------------------------------------------------------------------------

def get_struct_3d(file_path, skiprows=3):
    """
    Given a path to a .tec file, output a 1D arrays of ordered unique x, y, and z
    values, and 3D arrays of the associated real and imaginary data with
    dimension (nx, ny, nz)
    Usage: [x, y, z, real, imag] = get_3d_data('./avg_rhoda.tec')
    """
    if not os.path.isfile(file_path):
        raise IOError, "%s does not exist" % file_path
    # Load data from file
    [x, y, z, real, imag] = np.loadtxt(file_path, skiprows=skiprows).T
    # Get arrays of all unique x, y, and z values
    xset = np.unique(x)
    yset = np.unique(y)
    zset = np.unique(z)
    # Fix weird glitch where unique gives 2 0's
    if xset[0]==xset[1]:
        print "Weird 2 zero glitch happened, corrected"
        xset = xset[:-1]
    if yset[0]==yset[1]:
        yset = yset[:-1]
    if zset[0]==zset[1]:
        zset = zset[:-1]
    # Get dimensions of the data
    nx = xset.size
    ny = yset.size
    nz = zset.size
    # Reshape the data so we can refer to it like real_mat[ix, iy, iz]
    real_mat = real.reshape((nz, ny, nx)).T
    imag_mat = imag.reshape((nz, ny, nx)).T
    # Return data
    return struct_3d(xset, yset, zset, real_mat, imag_mat)

#------------------------------------------------------------------------------

def combine(in_dirs=None, prod_its=None, avg_files=None, out_dir='.'):
    """
    Input an array of relative directories 'in_dirs', an array of production
    iterations used to generate the files in those directories, and an array of
    .tec file names 'avg_files', and this function will take a weighted average
    of the 4th and 5th columns ('real' and 'imaginary') of each file name in
    each directory and output those weighted averages into new .tec files with
    the same names as those in 'avg_files' in the relative directory 'out_dir'.
    """
    if not in_dirs:
        raise ValueError, "in_dirs is empty"
    if not prod_its:
        raise ValueError, "prod_its is empty"
    # If prod_its is a scalar, assume each file had prod_its prod. iterations
    # and turn it into a single-valued list
    if not isinstance(prod_its, (list, tuple, np.ndarray)):
        print "Assuming each %d production iterations for each file" % prod_its
        # Create an array matching the length of in_dirs out of single value
        prod_its = [prod_its] * len(in_dirs)
    if len(in_dirs) != len(prod_its):
        raise ValueError, "lengths of in_dirs and prod_its don't match"
    if not avg_files:
        raise ValueError, "avg_files is empty"

    # Get number of data points from each file (assume they all match)
    f0 = os.path.join(in_dirs[0], avg_files[0])
    n_points = np.shape(np.loadtxt(f0, skiprows=3))[0]

    # Initialize numpy average arrays for each file
    rho_avg = [np.zeros(n_points) for f in avg_files]
    imag_avg = [np.zeros(n_points) for f in avg_files]

    # Accumulate real and imaginary density averages for each file
    for i_d, (d, its) in enumerate(zip(in_dirs, prod_its)):
        for i_f, fname in enumerate(avg_files):
            fpath = os.path.join(d, fname)
            [x, y, z, real, imag] = np.loadtxt(fpath, skiprows=3).T
            rho_avg[i_f] += real * its
            imag_avg[i_f] += imag * its

    # Divide by total number of production iterations to get average
    rho_avg = [rho / sum(prod_its) for rho in rho_avg]
    imag_avg = [imag / sum(prod_its) for imag in imag_avg]

    # Loop through the file names and create average files for each name
    for i_f, fname in enumerate(avg_files):
        # Get top 3 lines (.tec file header) of fname in first directory of
        # in_dirs (should be the same for a given fname in all directories)
        fpath_in = os.path.join(in_dirs[0], fname)
        fin = open(fpath_in, 'r')
        header_lines = list(islice(fin, 3))
        # Create file handle for output
        fpath_out = os.path.join(out_dir,fname)
        if os.path.isfile(fpath_out):
            raise IOError, "%s already exists. Exiting for safety." % fname
        fout = open(fpath_out, 'w')
        # Write header to output file
        fout.write(''.join(header_lines))
        # Get x, y, and z from one of the files of name fname. X, y, and z
        # should match in all of these files. The real and imag read in here
        # will just be discarded
        [x, y, z, real, imag] = np.loadtxt(fpath_in, skiprows=3).T
        data = np.matrix([x, y, z, rho_avg[i_f], imag_avg[i_f]]).T
        np.savetxt(fout, data, fmt='%.7e')
        # Close files
        fin.close()
        fout.close()

#------------------------------------------------------------------------------

def get_h_over_v(sim_dir='.', dim=3):
    """
    Given a directory 'sim_dir' to simulation data, return H/V
    """
    if not os.path.isdir(sim_dir):
        raise IOError, "%s is not a valid directory" % sim_dir
    # Get path to data.dat
    data_path = os.path.join(sim_dir, "data.dat")
    # Get H value from 2nd column of last line in data.dat
    H = os.popen("tail -1 %s" % data_path).read().split()[1]
    H = float(H)
    # Get Lx values from last field of lines of LOG that look like:
    #   Nx0: 35 Lx0: 4.700000
    #   Nx1: 147 Lx1: 25.000000
    #   Nx2: 147 Lx2: 25.000000
    Lx = np.empty(dim)
    for i in range(dim):
        cmd_str = "awk '/Lx%d:/ {print $NF}' %s/LOG" % (i, sim_dir)
        Lx[i] = os.popen(cmd_str).read()
    Lx = Lx.astype(np.float)
    # Multiply to get V and return H/V
    V = np.prod(Lx)
    return H / V

#------------------------------------------------------------------------------

def get_box_opt_data(pattern='*/', single=False, var=None):
    """
    Given an input pattern than matches only the directory names you want in
    the current directory, return an (n x 3) numpy matrix with columns
    [variable, boxlength, H/V] for each matched directory.
    """
    # Get list of subdirectories in current directory matching pattern
    dirlist = sorted([d for d in glob.glob(pattern) if os.path.isdir(d)])
    dirlist = [d[0:-1] if d[-1]=='/' else d for d in dirlist]
    # Get list of h/v values corresponding to each directory. Extra brackets
    # used to make a list of 1-element lists which will make a vertical numpy
    # array when placed in np.array()
    hv = [ [get_h_over_v(d)] for d in dirlist]
    if not single:
        # Assuming the directory names are of the form <variable>-<boxlength>,
        # split each directory into an array of [variable, boxlength]
        var_l = [d.split('-') for d in dirlist]
    else:
        var_l = [ [var, float(d)] for d in dirlist]
    # Make numpy array out of it and turn all values into floats instead of
    # strings
    var_l = np.array(var_l).astype(float)
    # Concatenate the (n x 2) var_l numpy array and (n x 1) hv array
    # horizontally to form an (n x 3) numpy array where the columns are
    # [variable, boxlength, H/V]
    var_l_hv = np.hstack((var_l, hv))
    return var_l_hv

#------------------------------------------------------------------------------

def plot_box_opt_data(var_l_hv, varname=r'$R_{np}$', varunits=r'$R_g$',
                      nlowpts=None, extend=[0.0, 0.0], save=True):
    import mpl_custom as mplc
    varset = np.sort(np.array(list(set(var_l_hv[:,0]))))
    for var in varset:
        l_hv = var_l_hv[np.where(var_l_hv[:,0] == var)][:,1:3]
        if nlowpts!=None:
            l_hv = l_hv[l_hv[:,1].argsort()]
        [x, y] = l_hv.T
        x_range = max(x) - min(x)
        x_fit = np.linspace(min(x) - extend[0]*x_range,
                            max(x) + extend[1]*x_range, 1000)
        if not nlowpts:
            p = np.poly1d(np.polyfit(x, y, 2))
        else:
            p = np.poly1d(np.polyfit(x[:nlowpts], y[:nlowpts], 2))
        y_fit = p(x_fit)
        print 'var =', var, ' x_min =', x_fit[np.argmin(y_fit)]
        lbl_data = varname + ' = ' + str(var) + varunits + ' data'
        plt.plot(x, y, 'o', label=lbl_data)
        lbl_fit = varname + ' = ' + str(var) + varunits +  ' fit'
        plt.plot(x_fit, y_fit, '-', label=lbl_fit)
    plt.xlabel(r'Box Width $\left[ R_g \right]$')
    plt.ylabel(r'$H/V$  $\left[k_BT/R_g^3 \right]$')
    plt.legend(loc='upper center')
    mplc.add_minorticks()
    if save:
        plt.savefig('box-opt-plot.png', bbox_inches='tight', dpi=120)


#=============================================================================#
#                       Constrained PMF Functions                             # 
#=============================================================================#


def get_h(sim_dir='.'):
    """
    Given a directory 'sim_dir' to simulation data, return H
    """
    if not os.path.isdir(sim_dir):
        raise IOError, "%s is not a valid directory" % sim_dir
    # Get path to data.dat
    data_path = os.path.join(sim_dir, "data.dat")
    # Get H value from 2nd column of last line in data.dat
    H = os.popen("tail -1 %s" % data_path).read().split()[1]
    return float(H)

#------------------------------------------------------------------------------

def get_constrained_pmf_data(pattern='*-*/', lz=None):
    """
    Given a directory matching pattern and the height of the simulation boxes,
    returns a ndir x 3 numpy array with (NP radius, NP surface-surface
    distance, H) as the columns. Directories must look like
    '<NP Radius>-<NP Position>/' where NP Position is the fraction of L_z of
    the center of particle 1 and the center of particle 2 is at (1-NP_Pos)*L_z
    """
    # Get sorted list of directories matching pattern
    dirlist = sorted(glob.glob(pattern))
    # Strip '/' off end of directories if it's there
    dirlist = [os.path.normpath(d) for d in dirlist]
    # Get H for each simulation
    h = np.array([get_h(d) for d in dirlist])
    # Get just '<NP Radius>-<NP Position>' from each directory
    rnp_pos = [os.path.basename(d) for d in dirlist]
    # Split each element of rnp_pos into NP Radius and NP Position arrays
    [rnp, pos] = np.array([rp.split('-') for rp in rnp_pos]).T.astype(float)
    # Get NP surface-surface distance from NP position array
    drminus2rnp = 2*(0.5-pos)*lz - 2*rnp
    # Shift H values so H=0 at farthest surface-surface distance
    for r in np.unique(rnp):
        # Get s-s distance subset for current NP radius
        dr_sub = drminus2rnp[np.where(rnp==r)]
        # Get H subset for same thing
        h_sub = h[np.where(rnp==r)]
        # Assign H_infinity as H associated with the largest s-s distance
        h_inf = h_sub[np.argmax(dr_sub)]
        # Subtract H_infinity from H's associated with current NP radius
        h[np.where(rnp==r)] -= h_inf
    # Combine NP radius, s-s distance, and H arrays into ndir x 3 numpy array
    rnp_dr_h = np.vstack((rnp, drminus2rnp, h)).T
    return rnp_dr_h

#------------------------------------------------------------------------------

def plot_constrained_pmf_data(rnp_dr_h=None, pattern='*-*/', lz=None,
                              varname=r'R_{NP}', varunits=r'R_g',
                              reverseLegend=False, alpha=1.0):
    """
    Plots constrained PMF. Either pass in sorted data with 3 columns
    (NP radius, NP surface-surface distance, H) or input directory matching
    pattern and L_z. Directories must look like '<NP Radius>-<NP Position>/'
    where NP Position is the fraction of L_z of the center of particle 1 and
    the center of particle 2 is at (1-NP_Pos)*L_z
    """
    # If data wasn't passed in, go get it
    if not rnp_dr_h:
        rnp_dr_h = get_constrained_pmf_data(pattern, lz)
    # Get the set of unique NP radius values from first column of data
    rnpset = np.unique(rnp_dr_h[:,0])
    # Reverse the order of rnpset if we want plots in reverse order
    if reverseLegend:
        rnpset = rnpset[::-1]
    # Set up custom line styles to cycle through
    # Get figure and axes handles to plot
    fig, ax = plt.subplots(1,1)
    for rnp in rnpset:
        # Get x (surface-surface distance) and y (H) for current NP radius
        [x, y] = rnp_dr_h[np.where(rnp_dr_h[:,0] == rnp)][:,1:3].T
        # Make label for legend
        lbl = r'$' + varname + r' = ' + str(rnp) + varunits + r'$'
        # Add curve for current NP radius to plot
        ax.plot(x, y, alpha=alpha, label=lbl)
    ax.set_xlabel(r'$\Delta r - 2R_{NP}$  $\left[ R_g \right]$')
    ax.set_ylabel(r'$\Delta F$  $\left[ k_BT\ \right]$')
    ax.legend(loc='best')
    mplc.add_minorticks(ax)
    return fig, ax

#------------------------------------------------------------------------------

def get_hybrid_pmf_data(sim_dirs, lz=None):
    pmf_data = []
    for i, d in enumerate(sim_dirs):
        fpath = os.path.join(d, 'rho_fld_np_c.tec')
        rnp = float(d)
        struct = get_struct_3d(fpath)
        [z, rho] = struct.get_midline()
        z = z - lz / 2 - 2 * rnp
        pmf = -np.log(rho)
        pmf -= pmf[-1]
        pmf_data.append([z, pmf])
    return pmf_data

#------------------------------------------------------------------------------

def get_cl_hybrid_pmf_data(sim_dirs, lz=None):
    pmf_data = []
    for i, d in enumerate(sim_dirs):
        fpath = os.path.join(d, 'avg_rho_fld_np_c.tec')
        rnp = float(d)
        struct = get_struct_3d(fpath)
        [z, rho] = struct.get_midline()
        z = z - lz / 2 - 2 * rnp
        pmf = -np.log(rho)
        pmf -= pmf[-1]
        pmf_data.append([z, pmf])
    return pmf_data

#------------------------------------------------------------------------------

def get_cl_maxcol_hybrid_pmf_data(sim_dirs, lz=None):
    pmf_data = []
    for i, d in enumerate(sim_dirs):
        fpath = os.path.join(d, 'avg_rho_fld_np_c.tec')
        rnp = float(d)
        struct = get_struct_3d(fpath).get_core()
        [z, rho] = [struct.z, np.max(struct.real, axis=(0,1))]
        z = z - lz / 2 - 2 * rnp
        pmf = -np.log(rho)
        pmf -= pmf[-1]
        pmf_data.append([z, pmf])
    return pmf_data

#------------------------------------------------------------------------------

def plot_hybrid_pmf_data(sim_dirs, pmf_data, lz=None, varname=r'$R_{NP}',
                         varunits=r'R_g$', save=True):
    #pmf_data = get_hybrid_pmf_data(sim_dirs, lz)
    rnpset = [float(d) for d in sim_dirs]
    for rnp, (z, pmf)  in zip(rnpset, pmf_data):
        lbl = varname + ' = ' + str(rnp) + varunits
        plt.plot(z, pmf, '-', label=lbl)
    plt.xlabel(r'$\Delta z - 2R_{NP}$  $\left[ R_g \right]$')
    plt.ylabel(r'$\Delta F$  $\left[ k_BT\ \right]$')
    plt.legend(loc='best')
    mplc.add_minorticks()
    if save:
        plt.savefig('pmf.png', bbox_inches='tight', dpi=120)

def plot_hybrid_pmf_data_nms(sim_dirs, pmf_data, lz=None, varname=r'$R_{P}',
                         varunits=r'nm$', save=True):
    #pmf_data = get_hybrid_pmf_data(sim_dirs, lz)
    rnpset = [float(d) for d in sim_dirs]
    for rnp, (z, pmf)  in zip(rnpset, pmf_data):
        lbl = varname + ' = ' + str(np.round(rnp*13.49, 1)) + varunits
        plt.plot(z*13.49, pmf, '-', label=lbl)
    plt.xlabel(r'$\Delta z - 2R_{P}$  [$nm$]')
    plt.ylabel(r'$\Delta F$  [$k_BT\ $]')
    plt.legend(loc='best')
    mplc.add_minorticks()
    if save:
        plt.savefig('pmf.png', bbox_inches='tight', dpi=120)

